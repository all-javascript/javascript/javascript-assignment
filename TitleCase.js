const titleCaseFunc = (str) => {
  str = str.toLowerCase().split(' ');
  for (var n = 0; n < str.length; n++) {
    str[n] = str[n].charAt(0).toUpperCase() + str[n].slice(1); 
  }
  return str.join(' ');
}
console.log(titleCaseFunc("hey there! sourav here"));
const first_element =  (array_of_numbers, k) => {
      if (array_of_numbers == null) 
      return 0;
    if (k == null) 
      return array_of_numbers[0];
    if (k < 0)
      return [];
    return array_of_numbers.slice(0, k);
  }

console.log(first_element([11, 10, 12, 24, 44, 52])); 
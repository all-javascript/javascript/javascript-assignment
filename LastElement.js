const last_element = (array_of_numbers, k) => {

  if (array_of_numbers == null) 
    return  0
  if (k == null) 
     return array_of_numbers[array_of_numbers.length - 1];
  return array_of_numbers.slice(Math.max(array_of_numbers.length - k, 0));  
  }

console.log("The Last Number Of The Array Is:", last_element([10, 22, 34, 42]));